if [ $# -ne 3 ]; then
    echo "USAGE: fight.sh <snake1_url> <snake2_url> <rounds>"
    echo "Check results in rounds.out"
    exit 1
fi

SNAKE1=$1
SNAKE2=$2
ROUNDS=$3

rm -f rounds.out

# Needs to install CLI https://github.com/BattlesnakeOfficial/rules
for i in $(seq $ROUNDS); do 
   ~/go/bin/battlesnake play --name $SNAKE1 --url $SNAKE1 --name $SNAKE2 --url $SNAKE2  --width 11 --height 11   2>&1 | tee -a rounds.out
done

cat rounds.out | grep winner |   awk '{ print $9 }' | sort | uniq -c

