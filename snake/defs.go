package snake

import (
	"encoding/json"
	"log"
	"math"
	"net/http"
)

type GameState struct {
	Game  Game        `json:"game"`
	Turn  int         `json:"turn"`
	Board Board       `json:"board"`
	You   Battlesnake `json:"you"`
}

type Game struct {
	ID      string  `json:"id"`
	Ruleset Ruleset `json:"ruleset"`
	Timeout int32   `json:"timeout"`
}

type Ruleset struct {
	Name     string   `json:"name"`
	Version  string   `json:"version"`
	Settings Settings `json:"settings"`
}

type Settings struct {
	FoodSpawnChance     int32  `json:"foodSpawnChance"`
	MinimumFood         int32  `json:"minimumFood"`
	HazardDamagePerTurn int32  `json:"hazardDamagePerTurn"`
	Royale              Royale `json:"royale"`
	Squad               Squad  `json:"squad"`
}

type Royale struct {
	ShrinkEveryNTurns int32 `json:"shrinkEveryNTurns"`
}

type Squad struct {
	AllowBodyCollisions bool `json:"allowBodyCollisions"`
	SharedElimination   bool `json:"sharedElimination"`
	SharedHealth        bool `json:"sharedHealth"`
	SharedLength        bool `json:"sharedLength"`
}

type Board struct {
	Height int           `json:"height"`
	Width  int           `json:"width"`
	Food   []Coord       `json:"food"`
	Snakes []Battlesnake `json:"snakes"`

	// Used in non-standard game modes
	Hazards []Coord `json:"hazards"`
}

func (b Board) Center() Coord {
	return Coord{X: (b.Width + 1) / 2, Y: (b.Height + 1) / 2}
}
func (b Board) MaxDiagonal() float64 {
	xx := float64(b.Width)
	yy := float64(b.Height)
	return math.Sqrt(xx*xx + yy*yy)
}
func (b Board) contains(c Coord) bool {
	return c.X >= 0 && c.X < b.Width && c.Y >= 0 && c.Y < b.Height
}
func (b Board) snakefree(c Coord) bool {
	for _, snake := range b.Snakes {
		if snake.Contains(c) {
			return false
		}
	}
	return true
}
func (b Board) SafeToGo(c Coord) bool {
	return b.contains(c) &&
		b.snakefree(c)
}
func (b Board) Corners() []Coord {
	return []Coord{
		{X: 0, Y: 0},
		{X: 0, Y: b.Height - 1},
		{X: b.Width - 1, Y: 0},
		{X: b.Width - 1, Y: b.Height - 1},
	}
}

// This is actually some sort of flood fill
// Exploring board from starting coordinates,
// Returning all reachable coordinate from start
func (b Board) Explore(start Coord, you Battlesnake) []Coord {

	var next []Coord
	var explored []Coord

	next = append(next, start)
	explored = append(explored, start)

	// While there are next coordinates to explore
	for len(next) > 0 {
		var current = next[0]
		next = next[1:]
		for _, nb := range current.Neighbours() {
			if b.SafeToGo(nb) && !Contains(explored, nb) && !you.Contains(nb) {
				next = append(next, nb)
				explored = append(explored, nb)
			}
		}
	}

	return explored
}

type Battlesnake struct {
	ID      string  `json:"id"`
	Name    string  `json:"name"`
	Health  int32   `json:"health"`
	Body    []Coord `json:"body"`
	Head    Coord   `json:"head"`
	Length  int32   `json:"length"`
	Latency string  `json:"latency"`

	// Used in non-standard game modes
	Shout string `json:"shout"`
	Squad string `json:"squad"`
}

// Check if snake contains coord c
func (s Battlesnake) Contains(c Coord) bool {
	for _, b := range s.Body[:len(s.Body)-1] {
		if b == c {
			return true
		}
	}
	return false
}

func (s Battlesnake) MightHeadButt(nextMove Coord) bool {
	head := s.Body[0]
    return Contains(head.Neighbours(), nextMove)
}

func (s Battlesnake) ReachableTail(explorable []Coord) bool {

	tail := s.Body[len(s.Body)-1]
	if Contains(explorable, tail) {
		return true
	}
	for _, nb := range tail.Neighbours() {
		if Contains(explorable, nb) {
			return true
		}
	}
	return false
}

type Coord struct {
	X int `json:"x"`
	Y int `json:"y"`
}

func (c Coord) Up() Coord    { return Coord{c.X, c.Y + 1} }
func (c Coord) Down() Coord  { return Coord{c.X, c.Y - 1} }
func (c Coord) Left() Coord  { return Coord{c.X - 1, c.Y} }
func (c Coord) Right() Coord { return Coord{c.X + 1, c.Y} }

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
func (c Coord) Dist(cc Coord) float64 {
	xx := float64(c.X - cc.X)
	yy := float64(c.Y - cc.Y)
	return math.Sqrt(xx*xx + yy*yy)
}

func (c Coord) Go(m Move) Coord {
	switch m {
	case Up:
		return c.Up()
	case Down:
		return c.Down()
	case Left:
		return c.Left()
	case Right:
		return c.Right()
	}
	return c
}

func (c Coord) Neighbours() []Coord {

	var result []Coord

	for _, m := range AllMoves() {
		result = append(result, c.Go(m))
	}
	return result
}

func Contains(coords []Coord, c Coord) bool {
	for _, cc := range coords {
		if cc == c {
			return true
		}
	}
	return false
}

// Response Structs

type BattlesnakeInfoResponse struct {
	APIVersion string `json:"apiversion"`
	Author     string `json:"author"`
	Color      string `json:"color"`
	Head       string `json:"head"`
	Tail       string `json:"tail"`
}

type BattlesnakeMoveResponse struct {
	Move  string `json:"move"`
	Shout string `json:"shout,omitempty"`
}

// HTTP Handlers

func HandleIndex(w http.ResponseWriter, r *http.Request) {
	response := info()

	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		log.Printf("ERROR: Failed to encode info response, %s", err)
	}
}

func HandleStart(w http.ResponseWriter, r *http.Request) {
	state := GameState{}
	err := json.NewDecoder(r.Body).Decode(&state)
	if err != nil {
		log.Printf("ERROR: Failed to decode start json, %s", err)
		return
	}

	start(state)

	// Nothing to respond with here
}

func HandleMove(w http.ResponseWriter, r *http.Request) {
	state := GameState{}
	err := json.NewDecoder(r.Body).Decode(&state)
	if err != nil {
		log.Printf("ERROR: Failed to decode move json, %s", err)
		return
	}

	response := move(state)

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		log.Printf("ERROR: Failed to encode move response, %s", err)
		return
	}
}

func HandleEnd(w http.ResponseWriter, r *http.Request) {
	state := GameState{}
	err := json.NewDecoder(r.Body).Decode(&state)
	if err != nil {
		log.Printf("ERROR: Failed to decode end json, %s", err)
		return
	}

	end(state)

	// Nothing to respond with here
}

// Middleware
