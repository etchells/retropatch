package snake

// This file can be a nice home for your Battlesnake logic and related helper functions.
//
// We have started this for you, with a function to help remove the 'neck' direction
// from the list of possible moves!

import (
	"log"
	"sort"
)

// This function is called when you register your Battlesnake on play.battlesnake.com
// See https://docs.battlesnake.com/guides/getting-started#step-4-register-your-battlesnake
// It controls your Battlesnake appearance and author permissions.
// For customization options, see https://docs.battlesnake.com/references/personalization
// TIP: If you open your Battlesnake URL in browser you should see this data.
func info() BattlesnakeInfoResponse {
	log.Println("INFO")
	return BattlesnakeInfoResponse{
		APIVersion: "1",
		Author:     "",        // TODO: Your Battlesnake username
		Color:      "#3E338F", // TODO: Personalize
		Head:       "scarf",   // TODO: Personalize
		Tail:       "present", // TODO: Personalize
	}
}

// This function is called everytime your Battlesnake is entered into a game.
// The provided GameState contains information about the game that's about to be played.
// It's purely for informational purposes, you don't have to make any decisions here.
func start(state GameState) {
	log.Printf("%s START\n", state.Game.ID)
}

// This function is called when a game your Battlesnake was in has ended.
// It's purely for informational purposes, you don't have to make any decisions here.
func end(state GameState) {
	log.Printf("%s END\n\n", state.Game.ID)
}

// This function is called on every turn of a game. Use the provided GameState to decide
// where to move -- valid moves are "up", "down", "left", or "right".
// We've provided some code and comments to get you started.
func move(state GameState) BattlesnakeMoveResponse {

	var myID int
	for n, sn := range state.Board.Snakes {
		if sn.ID == state.You.ID {
			myID = n
		}
	}
	gf := GameFrame{
		Board:   state.Board,
		MyID:    myID,
		Depth:   0,
		Food:    state.Board.Food,
		Hazards: state.Board.Hazards,
	}

	var possibleMoves = findMoveWithBetterExplorable(gf)

	// Have a default
	var nextMove = Up
	if len(possibleMoves) > 0 {
		nextMove = possibleMoves[0]
	}
	strs := []string{"up", "down", "left", "right"}
	return BattlesnakeMoveResponse{
		Move: strs[nextMove],
	}
}

func findMoveWithBetterExplorable(gf GameFrame) []Move {

	board := gf.Board
	snake := gf.Board.Snakes[gf.MyID]
	possibleMoves := gf.findSafeMoves(snake.Head)
	moveToReachable := map[Move]WeightedMove{}

	for _, move := range possibleMoves {
		gfNext := GameFrame{
			Board:   board,
			MyID:    gf.MyID,
			Depth:   0,
			Food:    gf.Food,
			Hazards: gf.Hazards,
		}
		nextPos := snake.Head.Go(move)
		food := gfNext.PlayMove(nextPos)
		foodScore := 0.
		if food {
			foodScore = board.MaxDiagonal()
		}
		moveToReachable[move] = WeightedMove{Score: gfNext.evaluate(nextPos) + foodScore, Coord: nextPos}
	}

	// Sorting these possible move according to how big the next explorable move will be
	sort.Slice(possibleMoves, func(i, j int) bool {
		return moveToReachable[possibleMoves[i]].Score > moveToReachable[possibleMoves[j]].Score
	})

	return possibleMoves
}
