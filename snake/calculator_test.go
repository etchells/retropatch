package snake

import (
	"sort"
	"testing"
    "encoding/json"
    "os"
    "io/ioutil"
)

func TestSuicidalShouldGoDown(t * testing.T) {

    jsonFile, _ := os.Open("tests/suicidal.json")
    
    var board Board
    byteValue, _ := ioutil.ReadAll(jsonFile)

    json.Unmarshal(byteValue, &board)

    myID := 0
    gf := GameFrame{
            Board: board,
            MyID:  myID,
            Depth: 0,
            Food:  board.Food,
    }
    my := board.Snakes[0]
    moveUp := my.Head.Go(Up)
    moveDown := my.Head.Go(Down)
    upScore := gf.evaluate(moveUp)
    downScore := gf.evaluate(moveDown)

    if upScore > downScore {
       t.Error("Stop being suicidal")
    }

}

/*

Goes up and dies -- but should have gone down

2022/02/17 13:05:17 Ruleset: solo, Seed: 1645099500913029000, Turn: 113
Hazards  : []
Food ●: [{0 4} {0 0}]
Sid ■■■: {6c7344d9-6947-4480-978d-20189a8c06b9 [{6 5} {5 5} {5 6} {4 6} {4 5} {3 5} {3 6} {2 6} {1 6} {0 6} {0 5} {1 5} {2 5} {2 4}] 91  0 }

*/

func TestSuicide(t *testing.T) {

	body := []Coord{
		{6, 5}, {5, 5}, {5, 6}, {4, 6}, {4, 5}, {3, 5}, {3, 6}, {2, 6}, {1, 6}, {0, 6}, {0, 5}, {1, 5}, {2, 5}, {2, 4},
	}
	snake := Battlesnake{
		Body: body,
		Head: body[0],
		ID:   "me",
	}
	snakes := []Battlesnake{snake}
	board := Board{
		Height: 7,
		Width:  7,
		Snakes: snakes,
		Food:   []Coord{},
	}
	myID := 0
	gf := GameFrame{
		Board: board,
		MyID:  myID,
		Depth: 5,
		Food:  []Coord{},
	}
	moveUp := body[0].Go(Up)
	moveDown := body[0].Go(Down)
	upScore := gf.evaluate(moveUp)
	downScore := gf.evaluate(moveDown)
	if upScore > downScore {
		t.Error("loser")
	}
}

func TestSort(t *testing.T) {
	mv2 := WeightedMove{
		Score: 0,
	}
	mv1 := WeightedMove{
		Score: -100,
	}
	mv3 := WeightedMove{
		Score: 1,
	}
	weightedMoves := []WeightedMove{mv1, mv2, mv3}

	sort.Slice(weightedMoves, func(i, j int) bool {
		return weightedMoves[i].Score > weightedMoves[j].Score
	})
	t.Log(weightedMoves)
}
