package snake

import "sort"

type Move int

const (
	Up Move = iota
	Down
	Left
	Right
)

func AllMoves() []Move {
	return []Move{Up, Down, Left, Right}
}

type WeightedMove struct {
	Coord Coord
	Score float64
}

type GameFrame struct {
	Board   Board
	MyID    int
	Depth   int
	Food    []Coord
	Hazards []Coord
}

func (gf *GameFrame) PlayMove(c Coord) bool {
	gf.Board.Snakes[gf.MyID].Head = c
	var food bool
	for n, f := range gf.Food {
		if c == f {
			food = true
			gf.Food = append(gf.Food[:n], gf.Food[n+1:]...)
			break
		}
	}
	body := gf.Board.Snakes[gf.MyID].Body
	if food {
		body = append([]Coord{c}, body...)
	} else {
		body = append([]Coord{c}, body[:len(body)-1]...)
	}
	gf.Board.Snakes[gf.MyID].Body = body
	return food
}

func (gf *GameFrame) evaluate(c Coord) float64 {
	if gf.Depth == 0 {
		return gf.heuristics(c)
	} else {
		// find valid moves
		valid := gf.findSafeMoves(c)
		if len(valid) < 1 {
			// bail early
			return -1000
		}
		weightedMoves := make([]WeightedMove, len(valid))
		for idx, mv := range valid {
			// play move on a copy of the board
			gfNext := GameFrame{
				Board:   gf.Board,
				Depth:   gf.Depth - 1,
				MyID:    gf.MyID,
				Food:    gf.Food,
				Hazards: gf.Hazards,
			}
			nextCoord := c.Go(mv)
			gfNext.PlayMove(nextCoord)
			weightedMoves[idx] = WeightedMove{Score: gfNext.evaluate(nextCoord), Coord: nextCoord}
		}
		// return the move with the hightest score
		sort.Slice(weightedMoves, func(i, j int) bool {
			return weightedMoves[i].Score > weightedMoves[j].Score
		})
		return weightedMoves[0].Score
	}
}

//See how liveshare works
func (frame *GameFrame) findSafeMoves(current Coord) []Move {
	you := frame.Board.Snakes[frame.MyID]
	board := frame.Board

	var possibleMoves = []Move{}

	for _, move := range AllMoves() {

		nextPos := current.Go(move)
		isSafe := board.SafeToGo(nextPos) && !you.Contains(nextPos)

		if isSafe {
			possibleMoves = append(possibleMoves, move)
		}
	}
	return possibleMoves
}

func (gf *GameFrame) heuristics(c Coord) float64 {

	board := gf.Board
	snake := board.Snakes[gf.MyID]

	reachableCoords := gf.Board.Explore(c, snake)
	reachableSize := float64(len(reachableCoords))

	reachableFoods := intersect(reachableCoords, board.Food)
	sort.Slice(reachableFoods, func(i, j int) bool {
		return reachableFoods[i].Dist(c) < reachableFoods[j].Dist(c)
	})

	reachableHazard := intersect(reachableCoords, board.Hazards)

	hazardValue := scale(0.0, float64(board.MaxDiagonal()), 1.0, 0.0, float64(len(reachableHazard)))

	closestFood := board.MaxDiagonal()
	if len(reachableFoods) > 0 {
		foodDist := reachableFoods[0].Dist(c)
		closestFood = board.MaxDiagonal() - foodDist
	}

	// Prefer to be in center.
	centerDist := float64(board.MaxDiagonal() - board.Center().Dist(c))*0.5

	// Prefer reachable tails
	reachableTail := 0.0
	if snake.ReachableTail(reachableCoords) {
		reachableTail = board.MaxDiagonal()
	}

	// Prefer not to headbutt larger or equal enemy
	noHeadButts := board.MaxDiagonal()
	for _, enemy := range board.Snakes {
		if enemy.ID != snake.ID && len(enemy.Body) >= len(snake.Body) && enemy.MightHeadButt(c) {
			noHeadButts = 0
			closestFood = 0
			centerDist = 0
			break
		}
	}

    enemySpace := 0
    for _, enemy := range board.Snakes {
		if enemy.ID != snake.ID {
            enemysReach := board.Explore(enemy.Head, enemy)
            enemySpace += len(enemysReach)
        } 
    }
   
    //Smaller enemy space the better.
    enemySpaceValue := float64(board.Height * board.Width - enemySpace)  * 0.5

	combinedHeuristic := reachableSize + centerDist + closestFood + reachableTail + 
                         hazardValue + noHeadButts + enemySpaceValue

    return float64(combinedHeuristic)

}

// min max -- input range
// from to -- output range
func scale(min, max, from, to, v float64) float64 {
	return (from - min) + v*(to-from)/(max-min)
}

func intersect(coordsA []Coord, coordsB []Coord) []Coord {
	var result []Coord

	for _, a := range coordsA {
		if Contains(coordsB, a) {
			result = append(result, a)
		}
	}

	return result
}
