package main

import (
	"log"
	"net/http"
	"os"

	"github.com/BattlesnakeOfficial/starter-snake-go/snake"
)

const ServerID = "BattlesnakeOfficial/starter-snake-go"

// Main Entrypoint

func main() {
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "8080"
	}

	http.HandleFunc("/", withServerID(snake.HandleIndex))
	http.HandleFunc("/start", withServerID(snake.HandleStart))
	http.HandleFunc("/move", withServerID(snake.HandleMove))
	http.HandleFunc("/end", withServerID(snake.HandleEnd))

	log.Printf("Starting Battlesnake Server at http://0.0.0.0:%s...\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func withServerID(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", ServerID)
		next(w, r)
	}
}
