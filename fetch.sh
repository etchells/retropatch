if [ $# -ne 2 ]; then
    echo "USAGE: fetch.sh gameID turn"
    echo "Example from share game link : https://play.battlesnake.com/g/91b83ae2-9242-40d9-accb-30f8765a1033/?turn=13"
    echo "Game ID will be 91b83ae2-9242-40d9-accb-30f8765a1033 turn will be 13"
    exit 1
fi


game=$1
turn=$2

curl "https://engine.battlesnake.com/games/$game/frames?offset=$turn&limit=1" | jq '.Frames[0] | {height: 11, width: 11, snakes: (.Snakes |  map({id: .Name, health: .Health, name: .Name, head: .Body[0],  body: .Body}) ), food: .Food, hazards: .Hazards}'
